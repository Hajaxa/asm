@echo off

set NAME=msgbox1

if exist %NAME%.obj del %NAME%.obj
if exist %NAME%.exe del %NAME%.exe

D:\\masm32\bin\ml /c /coff /nologo %NAME%.asm
D:\\masm32\bin\link /SUBSYSTEM:WINDOWS %NAME%.obj

dir %NAME%.*

pause
