; msgbox.asm
	
.386
.model flat, stdcall
option casemap:none

      include D:\\masm32\include\windows.inc
      include D:\\masm32\include\user32.inc
      include D:\\masm32\include\kernel32.inc

      includelib D:\\masm32\lib\user32.lib
      includelib D:\\masm32\lib\kernel32.lib

.data
	szWndTitle	db	"Wnd Title",0
	szWndText	db	"Wnd Text",0

       szTitle db "Salutations", 0
       szText db "Fenetre", 0
       
.code

start:

        push 0
        push offset szTitle
        push offset szText
        push 0
        call MessageBoxA
        
	jmp	_end

	push	MB_OK
	push	offset szWndTitle
	push	offset szWndText
	push	0
	call	MessageBoxA

_end:	
	push	0
	call	ExitProcess

end	start