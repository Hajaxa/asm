.386
.model flat, stdcall
option casemap:none

    include D:\\masm32\include\windows.inc
    include D:\\masm32\include\user32.inc
    include D:\\masm32\include\kernel32.inc

    includelib D:\\masm32\lib\user32.lib
    includelib D:\\masm32\lib\kernel32.lib


.data
hMapFile HANDLE 0
hFile db "msgbox2.exe", 0

.data?
hFileRead HANDLE ?
pMemory DWORD ?


.code
start:
    createfile:
        push NULL
        push FILE_ATTRIBUTE_ARCHIVE
        push OPEN_EXISTING
        push NULL
        push 0
        push GENERIC_READ OR GENERIC_WRITE
        push OFFSET hFile
        call CreateFile
        mov hFileRead, eax
        ;jmp next

    createfilemapping:
        push NULL
        push 0
        push 0
        push PAGE_READWRITE
        push NULL
        push hFileRead
        call CreateFileMapping
        mov hMapFile, eax
        
    mapviewoffile:
        push 0
        push 0
        push 0
        push FILE_MAP_READ OR FILE_MAP_WRITE
        push hMapFile
        call MapViewOfFile
        mov pMemory, eax

writeinfile:
          mov edi, pMemory
          add edi, 410H
  
      ;write ASCII "user32.dll",0
          mov eax, 72657375H
          stosd
          mov eax, 642E3233H
          stosd
          mov ax, 6C6CH
          stosw
          mov al, 00H
          stosb
  
      ;write ASCII "MessageBoxA"
          mov eax, 7373654DH
          stosd
          mov eax, 42656761H
          stosd
          mov eax, 0041786FH
          stosd
  

      ;write ASCII "Fenetre"
          mov eax, 656E6546H
          stosd
          mov eax, 00657274H
          stosd
  
      ;push ADDR "user32.dll"
          mov al, 68H
          stosb
          mov eax, 00401010H
          stosd


      ;call kernel32.LoadLibraryA
          mov al, 0E8H
          stosb
          mov eax, 73ED74C7H
          stosd
  
      ;push ADDR MessageBoxA
          mov al, 68H
          stosb
          mov eax, 0040101BH
          stosd
  
      ;push eax
          mov al, 50
          stosb

      ;call GetProcAddress
          mov al, 0E8H
          stosb
          mov eax, 73EC91BCH
          stosd

       ;push 0
            mov ax, 006AH
            stosw

        ;push "fenetre" title
            mov al, 68H
            stosb
            mov eax, 00401027H
            stosd

        ;push "fenetre" text
            mov al, 68H
            stosb
            mov eax, 00401027H
            stosd
            
       ;push 0
            mov ax, 006AH
            stosw

        ;call eax
            mov ax, 0FFD0H
            stosw

        ;jmp first instruction
            mov ax, 0EBAAH
            stosw

      ;target first instruction
          mov edi, pMemory
          add edi, 401H
  
      ;write jmp msgbox2.0040102C
          mov al, 2DH
          stosb
          
end start


    ;getfilesize:
        ;push NULL
        ;push hFileRead
        ;call GetFileSize
        
    ;unmapviewoffile:
    ;push pMemory
    ;call UnmapViewOfFile


        



;invoke GetFileSize,hFileRead,NULL
;invoke UnmapViewOfFile,pMemory
;call   CloseMapFile
;CloseMapFile PROC
;invoke CloseHandle,hMapFile
;mov    hMapFile,0
;invoke CloseHandle,hFileRead
;ret
;CloseMapFile endp

