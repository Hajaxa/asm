.386
.model flat, stdcall
option casemap:none

include C:\masm32\include\windows.inc
include C:\masm32\include\kernel32.inc
include C:\masm32\include\user32.inc

includelib C:\masm32\lib\kernel32.lib
includelib C:\masm32\lib\user32.lib

.data
    hTitle db "Injected", 0
    hText db "Injection successful", 0
    hFile db 'msgbox3.exe', 0

.data?
    hFileMap    HANDLE ?
    hFileRead   HANDLE ?
    pStartOfMap DWORD ?

.code

start:
    jmp     _createFile

_createFile:
    push    NULL
    push    FILE_ATTRIBUTE_ARCHIVE
    push    OPEN_EXISTING
    push    NULL
    push    0
    push    GENERIC_READ OR GENERIC_WRITE
    push    offset hFile
    call    CreateFile
    mov     hFileRead, eax

_createFileMapping:
    push    0
    push    0
    push    0
    push    PAGE_READWRITE
    push    NULL
    push    hFileRead
    call    CreateFileMapping
    mov     hFileMap, eax

_mapViewOfFile:
    push    0
    push    0
    push    0
    push    FILE_MAP_WRITE
    push    hFileMap
    call    MapViewOfFile
    mov     pStartOfMap, eax

_InjectPe:
    mov     edi, pStartOfMap
    add     edi, 176    ;moving to PE
    add     edi, 40     ;moving to entry point
    mov     al, 02Ah
    stosb
    mov     al, 010h
    stosb
    mov     al, 000h
    stosb
    mov     al, 000h
    stosb

    ;ASCII "userlib32.dll"
    mov     edi, pStartOfMap
    add     edi, 1038
    mov     al, 075h
    stosb
    mov     al, 073h
    stosb
    mov     al, 065h
    stosb
    mov     al, 072h
    stosb
    mov     al, 033h
    stosb
    mov     al, 032h
    stosb
    mov     al, 02Eh
    stosb
    mov     al, 064h
    stosb
    mov     al, 06Ch
    stosb
    mov     al, 06Ch
    stosb
    mov     al, 000h
    stosb

    ;ASCII "MessageBoxA"
    mov     al, 04Dh
    stosb
    mov     al, 065h
    stosb
    mov     al, 073h
    stosb
    mov     al, 073h
    stosb
    mov     al, 061h
    stosb
    mov     al, 067h
    stosb
    mov     al, 065h
    stosb
    mov     al, 042h
    stosb
    mov     al, 06Fh
    stosb
    mov     al, 078h
    stosb
    mov     al, 041h
    stosb
    mov     al, 000h
    stosb

    ;ASCII "Toto"
    mov     al, 054h
    stosb
    mov     al, 06Fh
    stosb
    mov     al, 074h
    stosb
    mov     al, 06Fh
    stosb
    mov     al, 000h
    stosb

    ;PUSH "User32.DLL"
    mov     al, 068h
    stosb
    mov     al, 00Eh
    stosb
    mov     al, 010h
    stosb
    mov     al, 040h
    stosb
    mov     al, 000h
    stosb

    ;CALL kernel32.LoadLibraryA
    mov     al, 0E8h
    stosb
    mov     al, 043h
    stosb
    mov     al, 039h
    stosb
    mov     al, 0CBh
    stosb
    mov     al, 074h
    stosb

    ;PUSH "MessageBoxA"
    mov     al, 068h
    stosb
    mov     al, 019h
    stosb
    mov     al, 010h
    stosb
    mov     al, 040h
    stosb
    mov     al, 000h
    stosb

    ;PUSH EAX
    mov     al, 050h
    stosb

    ;CALL kernel32.GetProcAddress
    mov     al, 0E8h
    stosb
    mov     al, 0E3h
    stosb
    mov     al, 001h
    stosb
    mov     al, 0CBh
    stosb
    mov     al, 074h
    stosb

    ;PUSH 0
    mov     al, 06Ah
    stosb
    mov     al, 000h
    stosb

    ;PUSH "toto"
    mov     al, 068h
    stosb
    mov     al, 025h
    stosb
    mov     al, 010h
    stosb
    mov     al, 040h
    stosb
    mov     al, 000h
    stosb

    ;PUSH "toto"
    mov     al, 068h
    stosb
    mov     al, 025h
    stosb
    mov     al, 010h
    stosb
    mov     al, 040h
    stosb
    mov     al, 000h
    stosb

    ;PUSH 0
    mov     al, 06Ah
    stosb
    mov     al, 000h
    stosb

    ;CALL EAX
    mov     al, 0FFh
    stosb
    mov     al, 0D0h
    stosb
    
    ;JMP SHORT msgbox2.00401002
    mov     al, 0EBh
    stosb
    mov     al, 0AFh
    stosb

_end:
    push    0
    push    offset hTitle
    push    offset hText
    push    0
    call    MessageBoxA
    push	0
    call	ExitProcess

end start