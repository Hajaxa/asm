.386
.model flat, stdcall
option casemap:none

    include C:\masm32\include\windows.inc
    include C:\masm32\include\user32.inc
    include C:\masm32\include\kernel32.inc

    includelib C:\masm32\lib\user32.lib
    includelib C:\masm32\lib\kernel32.lib


.data
hMapFile HANDLE 0
hFile db "msgbox1.exe", 0

.data?
hFileRead HANDLE ?
pMemory DWORD ?


.code
start:
    createfile:
        push NULL
        push FILE_ATTRIBUTE_ARCHIVE
        push OPEN_EXISTING
        push NULL
        push 0
        push GENERIC_READ OR GENERIC_WRITE
        push OFFSET hFile
        call CreateFile
        mov hFileRead, eax
        ;jmp next

    createfilemapping:
        push NULL
        push 0
        push 0
        push PAGE_READWRITE
        push NULL
        push hFileRead
        call CreateFileMapping
        mov hMapFile, eax
        
    mapviewoffile:
        push 0
        push 0
        push 0
        push FILE_MAP_READ OR FILE_MAP_WRITE
        push hMapFile
        call MapViewOfFile
        mov pMemory, eax

    writeinfile:
        mov edi, pMemory
        add edi, 428H

    ;write ASCII "Salutations",0
        ;mov eax, 53616C75H
        mov eax, 756C6153H
        stosd
        ;mov eax, 74617469H
        mov eax, 69746174H
        stosd
        ;mov eax, 6F6E7300H
        mov eax, 00736E6FH
        stosd

    ;write PUSH 0
        mov ax, 006AH
        stosw

    ;write PUSH msgbox1.00401028
        mov eax, 40102868H
        stosd
        mov al, 00H
        stosb

    ;write PUSH msgbox1.00401028
        mov eax, 40102868H
        stosd
        mov al, 00H
        stosb

    ;write PUSH 0
        mov ax, 006AH
        stosw

    ;write CALL user32.MessageBoxA
        mov eax, 05A7819E8H
        stosd
        mov al, 75H
        stosb

    ;write jmp msgbox1.00401002
        mov ax, 0B9EBH
        stosw

    ;target first instruction
        mov edi, pMemory
        add edi, 401H

    ;write jmp msgbox1.00401034
        mov al, 32H
        stosb
   
end start



;getfilesize:
;push NULL
;push hFileRead
;call GetFileSize    
;unmapviewoffile:
;push pMemory
;call UnmapViewOfFile
;invoke GetFileSize,hFileRead,NULL
;invoke UnmapViewOfFile,pMemory
;call   CloseMapFile
;CloseMapFile PROC
;invoke CloseHandle,hMapFile
;mov    hMapFile,0
;invoke CloseHandle,hFileRead
;ret
;CloseMapFile endp

